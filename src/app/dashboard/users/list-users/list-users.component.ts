import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../../shared/services/user.service';
import { UserService as ServiceComplete } from '../../services/user.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  allUsers: User[];
  filter = new FormControl('test');
  page = 1;
  pageSize = 5;
  collectionSize = 0;

  constructor(private userService: UserService, private serviceComplete: ServiceComplete, private router: Router) { }

  ngOnInit() {
    this.allUsers = [];
    this.userService.getUsers().subscribe((users: User[]) => {
      this.allUsers = users;
      this.collectionSize = this.allUsers.length;
    });
  }

  get usuarios(): User[] {
    return this.allUsers.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  onUpdate(id: number) {
    this.router.navigate(['dashboard/editar/', id]);
  }

  onDetail(id: number) {
    this.router.navigate(['dashboard/detalle/', id]);
  }

  onDelete(id: number) {
    this.serviceComplete.deleteUser(id).subscribe(() => {
      // this.allCategories.splice(this.allCategories.indexOf(category), 1);
      this.allUsers = this.allUsers.filter(c => c.id !== id);
      this.collectionSize = this.allUsers.length;
    });
  }

  onNewUser(){
    this.router.navigate(['dashboard/nuevo']);
  }

}
