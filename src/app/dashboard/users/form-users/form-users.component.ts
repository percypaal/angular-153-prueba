import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';

import { UserService } from '../../services/user.service';
import { User } from '../../../shared/models/user.model';
import { disableBindings } from '@angular/core/src/render3';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-form-users',
  templateUrl: './form-users.component.html',
  styleUrls: ['./form-users.component.css']
})
export class FormUsersComponent implements OnInit, OnChanges {

  @Input() objetoUsuario: User;
  @Input() tipo: string;
  editForm: FormGroup;
  user: User;
  loaded: boolean;

  constructor(private location: Location, private userService: UserService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['objetoUsuario']) {
      if (this.objetoUsuario) {
        this.createForm();
      }
    } else {
      this.iniciaForm();
    }
  }

  iniciaForm() {
    this.loaded = true;
    this.editForm = new FormGroup({
      name: new FormControl(
        '', [Validators.required, Validators.minLength(4)]
      ),
      lastname: new FormControl(
        '', [Validators.required, Validators.minLength(4)]
      ),
      email: new FormControl(
        '', [Validators.required, Validators.minLength(4)]
      ),
      phone: new FormControl(
        '', [Validators.required, Validators.minLength(4)]
      ),
      sexo: new FormControl(
        '', [Validators.required]
      ),
      status: new FormControl(
        '', [Validators.required]
      )
    });
  }

  createForm() {
    this.loaded = true;
    this.editForm = new FormGroup({
      name: new FormControl(
        this.objetoUsuario.name, [Validators.required, Validators.minLength(4)]
      ),
      lastname: new FormControl(
        this.objetoUsuario.lastname, [Validators.required, Validators.minLength(4)]
      ),
      email: new FormControl(
        this.objetoUsuario.email, [Validators.required, Validators.minLength(4)]
      ),
      phone: new FormControl(
        this.objetoUsuario.phone, [Validators.required, Validators.minLength(4)]
      ),
      sexo: new FormControl(
        this.objetoUsuario.sexo.toString(), [Validators.required]
      ),
      status: new FormControl(
        this.objetoUsuario.status.toString(), [Validators.required]
      )
    });
  }

  onSave() {
    if (this.editForm.invalid) {
      return false;
    }

    if (this.tipo === 'EDITAR') {
      const { name, lastname, email, phone, sexo, status } = this.editForm.value;
      const { id, password, created_at } = this.objetoUsuario;

      const user: User = {
        id,
        name,
        lastname,
        email,
        password,
        phone,
        sexo: +sexo,
        status: +status,
        updated_at: Date.now(),
        created_at
      };

      this.userService.updateUser(user)
        .subscribe(() => {
          this.onCancel();
        });
    } else {
      const { name, lastname, email, phone, sexo, status } = this.editForm.value;

      const user: User = {
        name,
        lastname,
        email,
        password: "123456",
        phone,
        sexo: +sexo,
        status: +status,
        updated_at: Date.now(),
        created_at: Date.now()
      };

      this.userService.createUser(user)
        .subscribe(() => {
          this.onCancel();
        });
    }

  }

  onCancel() {
    this.location.back();
  }

}
