import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '../../services/user.service';
import { User } from '../../../shared/models/user.model';

@Component({
  selector: 'app-detalle-users',
  templateUrl: './detalle-users.component.html',
  styleUrls: ['./detalle-users.component.css']
})
export class DetalleUsersComponent implements OnInit {
  user: User;

  constructor(private route: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUserById(id)
    .subscribe((user: User) => {
        this.user = user;
    })
  }

}
