import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { COMPONENTS } from './init';
import { UserService } from './services/user.service';
import { RoutingModule } from './routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { ListUsersComponent } from './users/list-users/list-users.component';
import { FormUsersComponent } from './users/form-users/form-users.component';
import { DetalleUsersComponent } from './users/detalle-users/detalle-users.component';
import { EditarUsersComponent } from './users/editar-users/editar-users.component';
import { NuevoUsersComponent } from './users/nuevo-users/nuevo-users.component';

@NgModule({
  declarations: [...COMPONENTS, ListUsersComponent, FormUsersComponent, DetalleUsersComponent, EditarUsersComponent, NuevoUsersComponent],
  imports: [CommonModule, RoutingModule, NgbModule, FormsModule, ReactiveFormsModule, SharedModule],
  exports: [...COMPONENTS],
  providers: [UserService]
})
export class DashboardModule { }
