import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from '../../shared/models/user.model';

@Injectable()
export class UserService {
    readonly BASE_URL: string = environment.api_url;
    readonly END_POINT: string = 'users';

    constructor(private http: HttpClient) {

    }

    getUsers2() {
        const API_URL = `${this.BASE_URL}/${this.END_POINT}`;

        return this.http.get(API_URL);
    }

    getUserById(id: number) {
        const API_URL = `${this.BASE_URL}/${this.END_POINT}/${id}`;
        return this.http.get(API_URL);
    }

    deleteUser(id: number) {
        const API_URL = `${this.BASE_URL}/${this.END_POINT}/${id}`;
        return this.http.delete(API_URL);
    }

    updateUser(user: User) {
        const API_URL = `${this.BASE_URL}/${this.END_POINT}/${user.id}`;

        return this.http.put(API_URL, user);
    }

    createUser(user: User) {
        const API_URL = `${this.BASE_URL}/${this.END_POINT}`;

        return this.http.post(API_URL, user);
    }

    getUsers() {
        return fetch('http://localhost:3000/users');
    }
}
