import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ListUsersComponent } from './users/list-users/list-users.component';
import { EditarUsersComponent } from './users/editar-users/editar-users.component';
import { DetalleUsersComponent } from './users/detalle-users/detalle-users.component';
import { NuevoUsersComponent } from './users/nuevo-users/nuevo-users.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        children: [
            {
                path: 'users',
                component: ListUsersComponent
            },
            {
                path: 'editar/:id',
                component: EditarUsersComponent
            },
            {
                path: 'detalle/:id',
                component: DetalleUsersComponent
            },
            {
                path: 'nuevo',
                component: NuevoUsersComponent
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoutingModule {}