export interface User {
    id?: number;
    name?: string;
    lastname?: string;
    email?: string;
    password?: string;
    phone?: string;
    sexo?: number;
    status?: number;
    created_at?: number;
    updated_at?: number;
}